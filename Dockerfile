# ------------------------------------------------------------------------------
# Start from the official Alpine Docker Image
# ------------------------------------------------------------------------------
FROM alpine:latest

RUN set -x && \
    # Upgrade the OS
    apk -U upgrade && \
    # Install LaTeX, Gnuplot, Graphviz, ImageMagick, OpenSCAD
    apk add --no-cache \
        vim \
        make \
        git \
        texlive \
        texmf-dist-latexextra \
        texmf-dist-pictures \
        texmf-dist-mathscience \
        gnuplot \
        graphviz \
        ttf-freefont \
        imagemagick \
        rsvg-convert \
        openscad && \
    # Clean cache
    rm -vfr /var/cache/apk/*
